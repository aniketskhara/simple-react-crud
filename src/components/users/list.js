import React, { useState, useEffect } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Fab from "@material-ui/core/Fab";
import Grid from "@material-ui/core/Grid";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import AddIcon from "@material-ui/icons/Add";
import IconButton from "@material-ui/core/IconButton";
import "react-notifications/lib/notifications.css";
import {
  NotificationContainer,
  NotificationManager
} from "react-notifications";

import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";

const columns = [
  { id: "name", label: "Name" },
  { id: "contactNumber", label: "Contact No" },
  { id: "region", label: "Region" },
  { id: "town", label: "Town" },
  { id: "postCode", label: "Post Code" },
  { id: "actions", label: "Actions" }
];

const useStyles = makeStyles(theme => ({
  modalPaper: {
    position: "absolute",
    width: "30%",
    backgroundColor: theme.palette.background.paper,
    boxshadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    margin: "2% 0 0 32%",
    height: "80%",
    borderRadius: "10px",
    border: "1px solid #0074d9"
  },
  root: {
    width: "100%",
    height: "650px",
    backgroundColor: "#ecf3f9"
  },
  container: {
    maxHeight: 440
  },
  addSubmit: {
    margin: "5% 0 0 40%",
    borderRadius: "10px",
    color: "#0074d9",
    border: "1px solid #0074d9"
  },
  heading: {
    fontWeight: 700,
    fontSize: "17px"
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  modaltitle: {
    textAlign: "center",
    color: "#0074d9"
  },
  tableContainer: {
    width: "85%",
    margin: "0 0 0 8%"
  },
  address: {
    width: "330px"
  },
  padding: {
    padding: "4% 0 0 0"
  },
  addButton: {
    backgroundColor: "#0074d9",
    margin: "0 0 0 23%"
  },
  mainHead: {
    color: "rgba(0, 0, 0, 0.87)"
  },
  iconButton: {
    color: "#000"
  },
  loader: {
    margin: "0% 0 0 47%"
  }
}));

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: "#0074d9",
    color: theme.palette.common.white,
    fontSize: "16px",
    fontWeight: 600
  },
  body: {
    fontSize: "16px",
    color: "rgba(0, 0, 0, 0.87)"
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  }
}))(TableRow);

const List = ({ getCount, drawerOpen, count }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [userData, setUserData] = useState(null);
  const [openEdit, setOpenEdit] = useState(false);
  const [isViewClicked, setIsViewClicked] = useState(false);
  const [loading, setLoading] = useState(false);

  const getUsers = async () => {
    setLoading(true);
    await axios
      .get(`https://haud-26b62.firebaseio.com/users.json`)
      .then(res => {
        mapUsers(res.data);
      })
      .catch(err => {});
  };

  const mapUsers = usersData => {
    let data = [];
    let customHash;
    Object.keys(usersData).map(function(key, index) {
      customHash = Object.assign({}, usersData[key], { id: key });
      data.push(customHash);
    });
    setUserData(data);
    getCount(data.length);
    setLoading(false);
  };

  useEffect(() => {
    getUsers();
  }, []);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const [userState, setUserState] = useState({
    firstName: "",
    lastName: "",
    address1: "",
    address2: "",
    town: "",
    region: "",
    country: "",
    postcode: "",
    contactNumber: ""
  });

  const clearStates = () => {
    setUserState({
      firstName: "",
      lastName: "",
      address1: "",
      address2: "",
      town: "",
      region: "",
      country: "",
      postcode: "",
      contactNumber: ""
    });
  };

  const handleUserChange = e => {
    const value = e.target.value;
    setUserState({
      ...userState,
      [e.target.name]: value
    });
  };

  const handleUserEditChange = e => {
    const value = e.target.value;
    setEditState({
      ...editstate,
      [e.target.name]: value
    });
  };

  const submitAddUserHandler = async () => {
    if (validate(userState)) {
      await axios
        .post("https://haud-26b62.firebaseio.com/users.json", userState)
        .then(res => {
          getUsers();
          clearStates();
          setOpen(false);
          NotificationManager.success("New User Added Successfully", "Success");
        })
        .catch(err => {
          NotificationManager.error("User Not Added", "Error");
        });
    } else {
      console.log("Invalid");
    }
  };

  const validate = filledData => {
    let err = [];
    let alphabets = /^[a-zA-Z]+$/;
    if (!alphabets.test(filledData.firstName)) {
      err.push("Please insert valid first name");
    }
    if (!alphabets.test(filledData.lastName)) {
      err.push("Please insert valid last name");
    }
    if (!/^[a-zA-Z0-9\s,'-]*$/.test(filledData.address1)) {
      err.push("Please insert valid address 1");
    }
    if (!/^[a-zA-Z0-9\s,'-]*$/.test(filledData.address2)) {
      err.push("Please insert valid address 2");
    }
    if (!alphabets.test(filledData.town)) {
      err.push("Please insert valid town");
    }
    if (!alphabets.test(filledData.region)) {
      err.push("Please insert valid region");
    }
    if (!alphabets.test(filledData.country)) {
      err.push("Please insert valid country");
    }
    if (!/^\d{5}$|^\d{5}-\d{4}$/.test(filledData.postcode)) {
      err.push("Please insert valid postcode");
    }
    if (
      !/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(
        filledData.contactNumber
      )
    ) {
      err.push("Please insert valid contact number");
    }
    if (err.length > 0) {
      let i;
      for (i = 0; i < err.length; i++) {
        NotificationManager.error(err[i], "Error");
      }
      return false;
    } else {
      return true;
    }
  };

  const submitEditUserHandler = id => {
    if (validate(editstate)) {
      axios
        .patch(`https://haud-26b62.firebaseio.com/users/${id}.json`, editstate)
        .then(res => {
          getUsers();
          setOpenEdit(false);
          NotificationManager.success("User Edited Successfully", "Success");
        })
        .catch(err => {
          NotificationManager.error("User not edited", "Error");
        });
    } else {
      console.log("Invalid");
    }
  };

  const deleteUser = async id => {
    await axios
      .delete(`https://haud-26b62.firebaseio.com/users/${id}.json`)
      .then(res => {
        getUsers();
        NotificationManager.success("User Deleted Successfully", "Success");
      })
      .catch(err => {
        NotificationManager.error("User not deleted", "Error");
      });
  };

  const [editstate, setEditState] = useState({
    firstName: "",
    lastName: "",
    address1: "",
    address2: "",
    town: "",
    region: "",
    country: "",
    postcode: "",
    contactNumber: "",
    id: ""
  });

  const EditUserHandler = item => {
    setOpenEdit(true);
    setIsViewClicked(false);
    setEditState({
      ...editstate,
      firstName: item.firstName,
      lastName: item.lastName,
      address1: item.address1,
      address2: item.address2,
      country: item.country,
      region: item.region,
      contactNumber: item.contactNumber,
      postcode: item.postcode,
      id: item.id,
      town: item.town
    });
  };

  const View = item => {
    setIsViewClicked(true);
    setOpenEdit(true);
    setEditState({
      ...editstate,
      firstName: item.firstName,
      lastName: item.lastName,
      address1: item.address1,
      address2: item.address2,
      country: item.country,
      region: item.region,
      contactNumber: item.contactNumber,
      postcode: item.postcode,
      id: item.id
    });
  };

  return (
    <React.Fragment>
      <NotificationContainer />
      {loading ? (
        <Loader
          type="ThreeDots"
          color="#0074d9"
          height={620}
          width={80}
          className={classes.loader}
          timeout={5000}
        />
      ) : (
        <Paper
          className={classes.root}
          style={
            drawerOpen === true ? { margin: "0 0 0 14%", width: "90%" } : {}
          }
        >
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <h1 className={classes.mainHead}>Users List</h1>
            </Grid>
            <Grid item xs={3}>
              <h4 className={classes.mainHead}> Total Users: {count}</h4>
            </Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={3}>
              <Fab
                color="primary"
                aria-label="add"
                className={classes.addButton}
                onClick={() => handleOpen()}
              >
                <AddIcon />
              </Fab>
            </Grid>
            <Grid item xs={12}>
              <TableContainer
                component={Paper}
                className={classes.tableContainer}
                boxshadow={3}
              >
                <Table className={classes.table} aria-label="customized table">
                  <TableHead>
                    <TableRow>
                      {columns.map(col => {
                        return (
                          <StyledTableCell align="center" key={col.id}>
                            {col.label}
                          </StyledTableCell>
                        );
                      })}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {userData &&
                      userData.map(row => {
                        return (
                          <StyledTableRow key={row.id}>
                            <StyledTableCell align="center">
                              {row.firstName + "  " + row.lastName}
                            </StyledTableCell>
                            <StyledTableCell align="center">
                              {row.contactNumber}
                            </StyledTableCell>
                            <StyledTableCell align="center">
                              {row.region}
                            </StyledTableCell>
                            <StyledTableCell align="center">
                              {row.town}
                            </StyledTableCell>
                            <StyledTableCell align="center">
                              {row.postcode}
                            </StyledTableCell>
                            <StyledTableCell align="center">
                              <IconButton
                                aria-label="view"
                                onClick={() => View(row)}
                              >
                                <VisibilityIcon
                                  titleAccess={"view"}
                                  className={classes.iconButton}
                                />
                              </IconButton>
                              <IconButton
                                aria-label="edit"
                                onClick={() => EditUserHandler(row)}
                              >
                                <EditIcon
                                  titleAccess={"edit"}
                                  className={classes.iconButton}
                                />
                              </IconButton>
                              <IconButton
                                aria-label="delete"
                                onClick={() => deleteUser(row.id)}
                              >
                                <DeleteIcon
                                  titleAccess={"delete"}
                                  className={classes.iconButton}
                                />
                              </IconButton>
                            </StyledTableCell>
                          </StyledTableRow>
                        );
                      })}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
          </Grid>

          <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={open}
            onClose={handleClose}
          >
            <div className={classes.modalPaper}>
              <h2 className={classes.modaltitle}>User Details</h2>
              <Grid container spacing={2} className={classes.padding}>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="First Name"
                    name="firstName"
                    value={userState.firstName}
                    onChange={e => handleUserChange(e)}
                  />
                </Grid>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Last Name"
                    name="lastName"
                    value={userState.lastName}
                    onChange={e => handleUserChange(e)}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} className={classes.padding}>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Address 1"
                    name="address1"
                    value={userState.address1}
                    onChange={e => handleUserChange(e)}
                    className={classes.address}
                  />
                </Grid>
              </Grid>
              <Grid container className={classes.padding}>
                <Grid item xs={12}>
                  <TextField
                    id="standard-helperText"
                    label="Address 2"
                    name="address2"
                    value={userState.address2}
                    onChange={e => handleUserChange(e)}
                    className={classes.address}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} className={classes.padding}>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Town"
                    name="town"
                    value={userState.town}
                    onChange={e => handleUserChange(e)}
                  />
                </Grid>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Region"
                    name="region"
                    value={userState.region}
                    onChange={e => handleUserChange(e)}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} className={classes.padding}>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Country"
                    name="country"
                    value={userState.country}
                    onChange={e => handleUserChange(e)}
                  />
                </Grid>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Post Code"
                    name="postcode"
                    value={userState.postcode}
                    onChange={e => handleUserChange(e)}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} className={classes.padding}>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Contact No"
                    name="contactNumber"
                    value={userState.contactNumber}
                    onChange={e => handleUserChange(e)}
                  />
                </Grid>
              </Grid>
              <Button
                variant="outlined"
                color="secondary"
                onClick={submitAddUserHandler}
                className={classes.addSubmit}
              >
                Add
              </Button>
            </div>
          </Modal>

          {/* Edit Modal */}
          <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={openEdit}
            onClose={() => {
              setOpenEdit(false);
            }}
          >
            <div className={classes.modalPaper}>
              <h2 className={classes.modaltitle}>User Details</h2>
              <Grid container spacing={2} className={classes.padding}>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="First Name"
                    name="firstName"
                    value={editstate.firstName}
                    onChange={e => handleUserEditChange(e)}
                    disabled={isViewClicked}
                  />
                </Grid>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Last Name"
                    name="lastName"
                    value={editstate.lastName}
                    onChange={e => handleUserEditChange(e)}
                    disabled={isViewClicked}
                  />
                </Grid>
              </Grid>
              <Grid container className={classes.padding}>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Address 1"
                    name="address1"
                    value={editstate.address1}
                    onChange={e => handleUserEditChange(e)}
                    disabled={isViewClicked}
                    className={classes.address}
                  />
                </Grid>
              </Grid>
              <Grid container className={classes.padding}>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Address 2"
                    name="address2"
                    value={editstate.address2}
                    onChange={e => handleUserEditChange(e)}
                    disabled={isViewClicked}
                    className={classes.address}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} className={classes.padding}>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Town"
                    name="town"
                    value={editstate.town}
                    onChange={e => handleUserEditChange(e)}
                    disabled={isViewClicked}
                  />
                </Grid>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Region"
                    name="region"
                    value={editstate.region}
                    onChange={e => handleUserEditChange(e)}
                    disabled={isViewClicked}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} className={classes.padding}>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Country"
                    name="country"
                    value={editstate.country}
                    onChange={e => handleUserEditChange(e)}
                    disabled={isViewClicked}
                  />
                </Grid>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Post Code"
                    name="postcode"
                    value={editstate.postcode}
                    onChange={e => handleUserEditChange(e)}
                    disabled={isViewClicked}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} className={classes.padding}>
                <Grid item xs>
                  <TextField
                    id="standard-helperText"
                    label="Contact No"
                    name="contactNumber"
                    value={editstate.contactNumber}
                    onChange={e => handleUserEditChange(e)}
                    disabled={isViewClicked}
                  />
                </Grid>
              </Grid>
              {isViewClicked ? (
                ""
              ) : (
                <Grid item xs>
                  <Button
                    variant="outlined"
                    color="secondary"
                    onClick={() => submitEditUserHandler(editstate.id)}
                    className={classes.addSubmit}
                  >
                    Edit
                  </Button>
                </Grid>
              )}
            </div>
          </Modal>
        </Paper>
      )}
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    count: state.users.userCount,
    drawerOpen: state.drawer.open
  };
};

const mapDispatchToProps = dispatch => ({
  getCount: users => dispatch({ type: "GET_COUNT", payload: users })
});

export default connect(mapStateToProps, mapDispatchToProps)(List);
