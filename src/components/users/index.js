import React, { Component } from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import ListItem from "./list";
import Header from "../header";

class Users extends Component {
  renderList = () => {
    return (
      <Paper elevation={3}>
        <Grid container>
          <ListItem />
        </Grid>
      </Paper>
    );
  };

  render() {
    return (
      <>
        <Header />
        {this.renderList()}
      </>
    );
  }
}

export default Users;
