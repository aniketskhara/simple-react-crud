import { combineReducers } from 'redux'
import usersReducer from './usersReducer'
import drawerReducer from './drawerReducer'

export default combineReducers({
  users: usersReducer,
  drawer: drawerReducer
});