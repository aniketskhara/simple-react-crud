const initialState = {
  open: false
};

const drawerReducer = (state = initialState, action) => {
  switch (action.type) {
    case "OPEN": {
      return {
        open: true
      };
    }
    case "CLOSE": {
      return {
        open: false
      };
    }
    default:
      return state;
  }
};

export default drawerReducer;
