const initialState = {
  userCount: 0
};

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case "GET_COUNT": {
      return {
        userCount: action.payload
      };
    }
    default:
      return state;
  }
};

export default usersReducer;
